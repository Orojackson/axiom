export class Note {

    id: number;
    complete_flag: boolean = false;
    title: string = '';
    comments: string = '';


    constructor(values: Object ={}){
      Object.assign(this, values);
    }

}

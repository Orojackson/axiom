import { Injectable } from '@angular/core';

import { Note } from './base';

@Injectable()
export class NoteDataService {

  last_id: number = 0;

  base_objects: Note[] = [];

  constructor() { }

  addNoteObject(base_object: Note): NoteDataService{

    if(!base_object.id){
      base_object.id = ++this.last_id;
    }
      this.base_objects.push(base_object);

      return this;
    }

  deleteNoteObjectById(id: number): NoteDataService{
    this.base_objects = this.base_objects.filter( base_object => base_object.id !== id);
    return this;
  }

  getAllNoteObjects(): Note[]{
    return this.base_objects;
  }

  getNoteObjectById(id: number): Note{
    return this.base_objects.filter(base_object => base_object.id === id).pop();
  }

  updateNoteObjectByID(id: number, values: Object= {}): Note{
    let base_object = this.getNoteObjectById(id);

    if(!base_object){
      return null;
    }

    Object.assign(base_object, values);

    return base_object;
  }

  toggleNoteObjectComplete(base_object: Note){
    let updated_base_object = this.updateNoteObjectByID(base_object.id, {
      complete_flag: !base_object.complete_flag
    });
    return updated_base_object;
  }

}

import { Component } from '@angular/core';
import { NoteDataService } from './base-data.service';
import { Note } from './base';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NoteDataService]
})
export class AppComponent {
  title: string =  'Task Guide';
  newNote: Note = new Note();

  constructor(private noteDataService: NoteDataService) {
  }

  addNoteObject(){
    this.noteDataService.addNoteObject(this.newNote);
    this.newNote = new Note();
  }

  toggleNoteObjectComplete(task){
    this.noteDataService.toggleNoteObjectComplete(task);
  }

  removeNoteObject(task){
    this.noteDataService.deleteNoteObjectById(task.id);
  }

  get tasks(){
    return this.noteDataService.getAllNoteObjects();
  }

}

import { Note } from './base';

describe('Note', () => {
  it('should create an instance', () => {
    expect(new Note()).toBeTruthy();
  });


it("should accept values in the constructor", () =>{
  let note = new Note({
    id: 1,
    complete_flag: false,
    title: 'write blog'
  });

  expect(note.title).toEqual('write blog');
  expect(note.complete_flag).toEqual(false);
});
});

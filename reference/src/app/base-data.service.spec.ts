import { TestBed, async, inject } from '@angular/core/testing';
import { Note } from './base';
import { NoteDataService } from './base-data.service';

describe('NoteDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoteDataService]
    });
  });

  it('should be created', inject([NoteDataService], (service: NoteDataService) => {
    expect(service).toBeTruthy();
  }));

describe('#getAllNoteObjects()', () => {

  it('should return an empty array by default',
  inject([NoteDataService], (service: NoteDataService) => {
    expect(service.getAllNoteObjects()).toEqual([]);
  }));

  it('should reuturn all base_objects',
  inject([NoteDataService], (service: NoteDataService) => {
    let base_object_1 = new Note({id:1, title:"abc1", complete_flag: false});
    let base_object_2 = new Note({id:2, title: 'abc2', complete_flag: true});

    service.addNoteObject(base_object_1);
    service.addNoteObject(base_object_2);

    expect(service.getAllNoteObjects()).toEqual([base_object_1,base_object_2]);
  }));
});

describe('#save(base_object)', () => {

  it("should automatically assign an incremnental id",
  inject([NoteDataService], (service: NoteDataService) => {
    let base_object_1 = new Note({title: "abc1", complete_flag: false});
    let base_object_2 = new Note({title: "abc2", complete_flag: true});

    service.addNoteObject(base_object_1);
    service.addNoteObject(base_object_2);

    expect(service.getNoteObjectById(1)).toEqual(base_object_1);
    expect(service.getNoteObjectById(2)).toEqual(base_object_2);
  }));
});

describe('#deleteNoteObjectById()', () => {

  it('should remove base_object by id',
    inject([NoteDataService], (service: NoteDataService) => {

      let base_object_1 = new Note({id: 1, complete_flag: false});
      let base_object_2 = new Note({id: 2, complete_flag: true});
      service.addNoteObject(base_object_1);
      service.addNoteObject(base_object_2);

      expect(service.getAllNoteObjects()).toEqual([base_object_1,base_object_2]);
      service.deleteNoteObjectById(1);
      expect(service.getAllNoteObjects()).toEqual([base_object_2]);
      service.deleteNoteObjectById(2);
      expect(service.getAllNoteObjects()).toEqual([]);
  }));

  it('should not removing anything if Note with corresponding id is not found', inject([NoteDataService], (service: NoteDataService) => {
      let Note1 = new Note({title: 'Hello 1', complete: false});
      let Note2 = new Note({title: 'Hello 2', complete: true});
      service.addNoteObject(Note1);
      service.addNoteObject(Note2);
      expect(service.getAllNoteObjects()).toEqual([Note1, Note2]);
      service.deleteNoteObjectById(3);
      expect(service.getAllNoteObjects()).toEqual([Note1, Note2]);
    }));

});

describe('#updateNoteById(id, values)', () => {

    it('should return base_object with the corresponding id and updated data', inject([NoteDataService], (service: NoteDataService) => {
      let base_object_1 = new Note({title: 'Hello 1', complete: false});
      service.addNoteObject(base_object_1);
      let updatedNote = service.updateNoteObjectByID(1, {
        title: 'new title'
      });
      expect(updatedNote.title).toEqual('new title');
    }));

    it('should return null if Note is not found', inject([NoteDataService], (service: NoteDataService) => {
      let base_object_1 = new Note({title: 'Hello 1', complete_flag: false});
      service.addNoteObject(base_object_1);
      let updatedNote = service.updateNoteObjectByID(2, {
        title: 'new title'
      });
      expect(updatedNote).toEqual(null);
    }));

});

describe('#toggleNoteObjectComplete(Note)', () => {

    it('should return the updated Note with inverse complete status', inject([NoteDataService], (service: NoteDataService) => {
      let base_object_1 = new Note({title: 'Hello 1', complete_flag: false});
      service.addNoteObject(base_object_1);
      let updatedNote = service.toggleNoteObjectComplete(base_object_1);
      expect(updatedNote.complete_flag).toEqual(true);
      service.toggleNoteObjectComplete(base_object_1);
      expect(updatedNote.complete_flag).toEqual(false);
    }));
  });
});

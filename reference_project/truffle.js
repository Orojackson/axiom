var DefaultBuilder = require("truffle-default-builder");

module.exports = {
  build: new DefaultBuilder({
    "homepage.html": "homepage.html",
    "property.js": ["javascripts/property.js"],"property.css": ["stylesheets/property.css"]
  }),
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    }
  }
};

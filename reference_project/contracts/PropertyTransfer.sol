pragma solidity ^0.4.13;

contract PropertyTransfer {

    // Used to store user addresses ---> number of property token hold by the address    
    mapping (string=>details) property_OwnerMapping; // just like mapping voter address with his details
    
    struct details {
        address owner;
        string property_name;
        uint cost;
        bool isAvailableForSale;
        uint floors;
        string area;
        string propertyaddress;
    } 
    
    details[10] public propertyDetail; // array of struct
    uint i;
    
    address contract_owner;
    
    function PropertyTransfer(){
    
    contract_owner = msg.sender;
    
    }
   
    function PropertyRegistration(string _property_name,address _owner,uint _cost,bool _isAvailableForSale,uint _floors,string _area,string _address){
    
          propertyDetail[i].property_name= _property_name;
          propertyDetail[i].owner = _owner;
          propertyDetail[i].cost = _cost;
          propertyDetail[i].isAvailableForSale= _isAvailableForSale;
          propertyDetail[i].floors = _floors;
          propertyDetail[i].area = _area;
          propertyDetail[i].propertyaddress = _address;
          property_OwnerMapping[_property_name] = propertyDetail[i]; //Mapping the propertyname with Propertydetail strcuct
          i++;
          
    }
    
    modifier onlyAdmin() {
            if (contract_owner!=msg.sender)
             revert();
            _;
    }
    
    function AuthorizeProperty(string propertyname)
        {
            if(msg.sender!=contract_owner){
            throw; //Reverts back to initial state and ether
        }
             property_OwnerMapping[propertyname] = propertyDetail[i];
             i++;
        }
    
    function getCounter() constant returns (uint){
            return i;
    }
    
    
function getPropertyList(uint x) constant returns(address,string,uint,bool,uint,string,string)
    {
       return ( propertyDetail[x].owner,
                       propertyDetail[x].property_name,
                       propertyDetail[x].cost,
                       propertyDetail[x].isAvailableForSale,
                       propertyDetail[x].floors,
                       propertyDetail[x].area,
                       propertyDetail[x].propertyaddress
                       );
    
    }
}
